package com.night.chalenge2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.night.chalenge2.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.apply {
            ivList.setOnClickListener {
                ivList.visibility = View.GONE
                ivGrid.visibility = View.VISIBLE

                icluGridLayout.root.visibility = View.VISIBLE
                icluListLayout.root.visibility = View.GONE
            }
            ivGrid.setOnClickListener {
                ivList.visibility = View.VISIBLE
                ivGrid.visibility = View.GONE

                icluGridLayout.root.visibility = View.GONE
                icluListLayout.root.visibility = View.VISIBLE
            }

        }
    }
}